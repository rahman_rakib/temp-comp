EESchema Schematic File Version 4
LIBS:pos_cap-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Capacitors"
Date "19 oct 2012"
Rev "1.0"
Comp "WASHU AIMLAB"
Comment1 "Rakib"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:VCC #PWR01
U 1 1 5080AA9F
P 8500 200
F 0 "#PWR01" H 8500 50  50  0001 C CNN
F 1 "VCC" H 8500 350 50  0000 C CNN
F 2 "" H 8500 200 50  0000 C CNN
F 3 "" H 8500 200 50  0000 C CNN
	1    8500 200 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5CF552C9
P 6150 4050
F 0 "C1" H 6265 4096 50  0000 L CNN
F 1 "C" H 6265 4005 50  0000 L CNN
F 2 "aimlib:CAP_SMD_1111(2828Metric)" H 6188 3900 50  0001 C CNN
F 3 "~" H 6150 4050 50  0001 C CNN
	1    6150 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5CF55E2E
P 6600 4050
F 0 "C2" H 6715 4096 50  0000 L CNN
F 1 "C" H 6715 4005 50  0000 L CNN
F 2 "aimlib:CAP_SMD_1111(2828Metric)" H 6638 3900 50  0001 C CNN
F 3 "~" H 6600 4050 50  0001 C CNN
	1    6600 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5CF56803
P 7300 4050
F 0 "C3" H 7415 4096 50  0000 L CNN
F 1 "C" H 7415 4005 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7338 3900 50  0001 C CNN
F 3 "~" H 7300 4050 50  0001 C CNN
	1    7300 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 3900 6600 3900
Wire Wire Line
	6150 4200 6600 4200
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5CF571D0
P 5750 4000
F 0 "J1" H 5550 4000 50  0000 C CNN
F 1 "Conn_01x02_Male" H 5500 4100 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5750 4000 50  0001 C CNN
F 3 "~" H 5750 4000 50  0001 C CNN
	1    5750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 4000 5950 3900
Wire Wire Line
	5950 3900 6150 3900
Connection ~ 6150 3900
Wire Wire Line
	5950 4100 5950 4200
Wire Wire Line
	5950 4200 6150 4200
Connection ~ 6150 4200
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 5CF57F22
P 7750 4000
F 0 "J4" H 8200 3900 50  0000 C CNN
F 1 "Conn_01x02_Male" H 8400 4000 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7750 4000 50  0001 C CNN
F 3 "~" H 7750 4000 50  0001 C CNN
	1    7750 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 4000 7950 3900
Wire Wire Line
	7950 3900 7300 3900
Wire Wire Line
	7950 4100 7950 4200
Wire Wire Line
	7950 4200 7300 4200
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5CF87778
P 6850 4500
F 0 "J2" H 6650 4500 50  0000 C CNN
F 1 "Conn_01x02_Male" H 6600 4600 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6850 4500 50  0001 C CNN
F 3 "~" H 6850 4500 50  0001 C CNN
	1    6850 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5CF8921F
P 7550 4600
F 0 "J3" H 7350 4600 50  0000 C CNN
F 1 "Conn_01x02_Male" H 7300 4700 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7550 4600 50  0001 C CNN
F 3 "~" H 7550 4600 50  0001 C CNN
	1    7550 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	7050 4500 7350 4500
Wire Wire Line
	7050 4600 7350 4600
$EndSCHEMATC
